package com.ruoyi.system.convert;

import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.system.domain.vo.RouterVo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface SysUserConvert {

    SysUserConvert INSTANCE = Mappers.getMapper(SysUserConvert.class);


    @Mapping(source = "updateTime", target = "updateTime", ignore = true) // 字段相同，但是含义不同，忽略
    RouterVo convert0(LoginUser bean);
}
